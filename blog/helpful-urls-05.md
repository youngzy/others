# 分享一些有用的网站/网页（五）

某一天忽然冒出个想法：把曾经对我有过帮助的网站或网页记录一下。以博客的方式，相当于投票吧，增加其PR。
最直接的应该是点赞之类的。但这样做的前提是必须要有个账号。为了点赞去注册一个账号，不合适。所以另辟蹊径，想了这么一个办法。

> 说明：
> - 各类别按时间顺序添加，排名不分先后
> - 记录的是下次可能还要查的信息。那种查一下知道就行了的没有记录
> - 每篇分享10个
>

## 技术类（6）
1. [https://www.cnblogs.com/wowpH/p/11687428.html](https://www.cnblogs.com/wowpH/p/11687428.html)  
    Java 正则表达式：获取中括号之间的内容。
2. https://www.cnblogs.com/developer_chan/p/8577351.html  
    在Intellij IDEA中反向工程生成Hibernate的配置文件和实体类。  
    如果View-ToolWindows下没有 Persistence，进入Project Settings-Modules 添加JPA或者Hibernate。
	
	

## 工具类（3）
1. https://processon.com/  
    流程图、架构图、UML等在线画图，还可以自定义水印。


## 其他（1）
1. 



