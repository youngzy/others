# 分享一些有用的网站/网页（三）

某一天忽然冒出个想法：把曾经对我有过帮助的网站或网页记录一下。以博客的方式，相当于投票吧，增加其PR。
最直接的应该是点赞之类的。但这样做的前提是必须要有个账号。为了点赞去注册一个账号，不合适。所以另辟蹊径，想了这么一个办法。

> 说明：
> - 各类别按时间顺序添加，排名不分先后
> - 记录的是下次可能还要查的信息。那种查一下知道就行了的没有记录
> - 每篇分享10个
>

## 技术类（4）
1. [https://blog.csdn.net/songzehao/article/details/80686663](https://blog.csdn.net/songzehao/article/details/80686663)

   路径匹配的工具类：AntPathMatcher 。

2. [https://blog.csdn.net/hebeixmg/article/details/72902511](https://blog.csdn.net/hebeixmg/article/details/72902511)

      `Thread.currentThread().getContextClassLoader().getResource("")`，当不确定路径指向哪时可以这样试试。

      注：直接用空串，不要给一个不存在的值。你以为它会抛出文件找不到的异常，实际上你会得到一个`null`。
      
3. [https://juejin.im/post/5bce68226fb9a05ce46a0476#heading-12](https://juejin.im/post/5bce68226fb9a05ce46a0476#heading-12)

      Java的值传递和引用传递，详细透彻，有图。
      
4. [https://blog.csdn.net/qq_41267618/article/details/89819593](https://blog.csdn.net/qq_41267618/article/details/89819593)

      JSP嵌套JSP。三种方法之一：`<%@ include file="xx.jsp"%>`

5. [https://learngitbranching.js.org/](https://learngitbranching.js.org/)

      趣味学习Git
      
6. [http://www.mamicode.com/info-detail-2347253.html](http://www.mamicode.com/info-detail-2347253.html)

      Hibernate对象三种状态：瞬时态（Transient）、持久态（Persistent）、脱管/游离态（Detached）
 
7. [https://juejin.im/post/6844904193170341896](https://juejin.im/post/6844904193170341896)

   `Failed to connect to github.com port 443: Operation timed out`如果你在使用IDEA（Mac）时遇到这个问题，99.99%可以帮你解决。简单说就是要改hosts文件，详细的请查看链接。
   
8. [https://sourcemaking.com/design_patterns](https://sourcemaking.com/design_patterns)

   详解设计模式。虽然是英文版，但是有例子啊。有例子！！！

## 生活类



## 其他

1. [https://baijiahao.baidu.com/s?id=1658118868640123446](https://baijiahao.baidu.com/s?id=1658118868640123446)

   Excel一次插入多行或多列。好用，居然还有动态图片的演示。
   
2. [https://list.yinxiang.com/](https://list.yinxiang.com/)

   印象笔记的各种模板。



