# 分享一些有用的网站/网页（四）

某一天忽然冒出个想法：把曾经对我有过帮助的网站或网页记录一下。以博客的方式，相当于投票吧，增加其PR。
最直接的应该是点赞之类的。但这样做的前提是必须要有个账号。为了点赞去注册一个账号，不合适。所以另辟蹊径，想了这么一个办法。

> 说明：
> - 各类别按时间顺序添加，排名不分先后
> - 记录的是下次可能还要查的信息。那种查一下知道就行了的没有记录
> - 每篇分享10个
>

## 技术类（6）
1. [https://www.cnblogs.com/junhuawang/p/6952748.html](https://www.cnblogs.com/junhuawang/p/6952748.html)  
	如果在IDEA中运行JUnit时遇到类似这样的错误：`java.lang.NoClassDefFoundError: org/hamcrest/SelfDescribing`，那就添加对应jar包。  
	具体步骤如下：菜单`File->Project Structure->Module->Dependencies`，从`IDEA的安装目录\lib`选择相关jar。
2. [https://juejin.im/entry/6844903788738772999](https://juejin.im/entry/6844903788738772999)  
	Maven 测试（test）指定的类。
3. [https://www.cnblogs.com/wang1001/p/9768029.html](https://www.cnblogs.com/wang1001/p/9768029.html)  
	Java生成随机数的工具类。
4. [https://www.cnblogs.com/bill89/p/11044928.html](https://www.cnblogs.com/bill89/p/11044928.html)  
	Oracle Job的实例与详解。
5. [https://www.cnblogs.com/jiading/articles/13193992.html](https://www.cnblogs.com/jiading/articles/13193992.html)  
	Java printf 的用法。
6. [https://www.cnblogs.com/mengchunchen/p/9669704.html](https://www.cnblogs.com/mengchunchen/p/9669704.html)  
	 查看Linux系统的CPU等的使用情况使用 `top` 命令。
	
	

## 工具类（3）
1. [https://www.cnblogs.com/EasonJim/archive/2004/01/13/7867472.html](https://www.cnblogs.com/EasonJim/archive/2004/01/13/7867472.html)  
	IntelliJ IDEA 如何使 Properties 文件显示中文？  
	File -> Settings -> Editor -> File Encodings，Properties Files，Transparent native-to-ascii conversion，打勾。
2. [http://791202.com/2020/03/12/intellij-idea/588/](http://791202.com/2020/03/12/intellij-idea/588/)  
	在 IntelliJ IDEA 中 commit 时会有 “Checking for line separator...”，如果想去掉，就看看这个。  
	可能只有很卡时才会发现。
	
3. [https://www.123cha.com/ip/](https://www.123cha.com/ip/)  
	IP信息查询。


## 其他（1）
1. [http://www.dll-files.com](http://www.dll-files.com)  
	Windows 系统缺dll时可以来这里看看。



