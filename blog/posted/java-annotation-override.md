title: @Override 千万不要再把它当成可有可无的了
date: 2020-04-18 10:58:15
categories:
tags:
permalink: java-annotation-override
---
`@Override`，一定不陌生吧！就是没敲过，也一定见过。
是不是觉得它可有可无，甚至是个累赘？​
不爱搭理它就算了。IDE帮你生成了你还要删掉它，这就有点过分了哈。

它真的是毫无用处吗？开发JDK和IDE的大神们造了个无用的东西？
​​​​
​看看下面这个例子，请你来找茬儿。
多它不多，少它不少。

IShape类
```java
public interface IShape {
    String introduceYourself();
}
```
Rectangle类

```java
public class Rectangle implements IShape {
    public String introduceYourself() {
        return "我是一个长方形。";
    }
}
```

Square类

```java
public class Square extends Rectangle {
    public String introduceYouself() {
        return "你可以叫我长方形，但它并不是我的真名。我的真名叫正方形。";
    }
}
```

测试类 测试结果

这是为什么呢？
明明是一个正方形，它怎么偏说自己是个长方形呢？​​
能看出是哪的问题吗？
如果还是找不到问题，在方法上添加​​​`@Override`注解看看。
是不是很神奇？

所以，请善待它吧！
其实之前我也没认识到这一点。是最近听了一个关于 Annotation的课才知道它还有这个用处。​

### 相关链接
- [GitHub](https://github.com/youngzhu/CodeLab/tree/master/src/main/java/com/youngzy/stackskills/ral/annotation)



