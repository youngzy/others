# 分享一些有用的网站/网页（二）

某一天忽然冒出个想法：把曾经对我有过帮助的网站或网页记录一下。以博客的方式，相当于投票吧，增加其PR。
最直接的应该是点赞之类的。但这样做的前提是必须要有个账号。为了点赞去注册一个账号，不合适。所以另辟蹊径，想了这么一个办法。

> 说明：
> - 各类别按时间顺序添加，排名不分先后
> - 记录的是下次可能还要查的信息。那种查一下知道就行了的没有记录
> - 每篇分享10个
>

## 技术类（4）
1. [https://blog.csdn.net/weixin_40553655/article/details/102832224](https://blog.csdn.net/weixin_40553655/article/details/102832224)

   用Spring的md5加密时，相同的代码在不同环境运行会有不同的结果？加密字符串中含有中文等特殊字符吧？指定字符集就可以了。`DigestUtils.md5DigestAsHex(str.toString().getBytes("UTF-8")));` 。不管有没有特殊字符，还是统一字符集比较好。谁知道什么时候出幺蛾子。

2. [https://www.cnblogs.com/cc11001100/p/6883790.html](https://www.cnblogs.com/cc11001100/p/6883790.html)

   Hibernate中 `get()` 和 `load()` 的区别。文中这句总结的很好：get()是用于不确定对象是否真的存在的情况，所以在查询出来后可以先进行一个空指针判断；而load()方法用于对象一定存在的情况下，不然等会儿使用的时候就可能会抛出ObjectNotFoundException了。
   
3. [https://blog.csdn.net/u014532901/article/details/78820124](https://blog.csdn.net/u014532901/article/details/78820124)

   Java Set  和 List 的互转。如 `new ArrayList<>(xxSet)`
   
 4. [http://cslibrary.stanford.edu/](http://cslibrary.stanford.edu/)  
      斯坦福大学，数据结构相关的学习资料。
      
 5. [https://blog.csdn.net/qq_38132283/article/details/106339289](https://blog.csdn.net/qq_38132283/article/details/106339289)

       Maven 引入 JavaFX

6. [https://www.cnblogs.com/hepengju/p/11610451.html](https://www.cnblogs.com/hepengju/p/11610451.html)

      Maven 的 setting.xml 文件。例如可以将中央仓库改成阿里的，这样更容易下载依赖包，不会有失败或超时的情况。
      
7. [https://blog.csdn.net/lmy86263/article/details/60479350](https://blog.csdn.net/lmy86263/article/details/60479350)
   
   Java中InputStream和String之间的转换。
   
8. [https://blog.csdn.net/gnail_oug/article/details/80005957](https://blog.csdn.net/gnail_oug/article/details/80005957)

      Oracle 一次插入（insert）多条记录

9. [https://cloud.tencent.com/developer/article/1177624](https://cloud.tencent.com/developer/article/1177624)

      Java enum  的使用。包括继承等。其实不能使用`extends`，可以使用 `implements`

## 生活类

## 工具/App类

## 其他

1. [https://blog.csdn.net/weixin_44754740/article/details/104495148](https://blog.csdn.net/weixin_44754740/article/details/104495148)

​	想一边听课一边做其他的结果网站不允许？ **F12 - Event Listeners - blur**，将其子项 **Remove** 。（Google Chrome 版本 83.0.4103.116（64 位））

​	如果没有看到这一项（包括原文中的“mouseout”），则 **右键 - 重新加载**。

​	真没想到，调试模式还有这用处。就像网友说的：奇怪的知识增加了。



