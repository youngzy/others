# 标题：APP推荐（一）

一来确实是推荐。

二来也是为了记录。以前满足过我需求的App，一段时间不用后，就想不起来是哪个了。又要重新去找重新去试。

## 1. Typora

Windows下好用且**免费**的Markdown工具。

以前用的时候没太注意。换电脑需要重新下载，再次点开官网（网站也挺漂亮），发现了很多隐藏技能，如文档结构，目录树，甚至还能画流程图等。

官方网站：[https://www.typora.io/](https://www.typora.io/)

## 2. 最美证件照
拍证件照，可以换背景换装等。iOS和Android都有，关键还免费。

## 3. 福昕阅读器

PDF阅读器，好用，还可编辑。暂时没用到其他功能，这两个就够了。

下载地址：[www.foxitsoftware.cn](https://www.foxitsoftware.cn/pdf-reader/)

## 4. 坚果云

可在不同设备不同环境（家&公司）间都可查看和修改资料。刚开始用，挺不错的。同步贼快。

官网：[https://www.jianguoyun.com](https://www.jianguoyun.com)

## 5. Sidekick
作为Chrome的替代被推荐的，据说不那么吃内存。但在Windows上比较了一下，没有明显的差别。Chromium内核。外观还是挺漂亮的。

下载地址：[https://www.meetsidekick.com/download](https://www.meetsidekick.com/download)
