# 一些有用的网站/网页（一）

某一天忽然冒出个想法：把曾经对我有过帮助的网站或网页记录一下。以博客的方式，相当于投票吧，增加其PR。
最直接的应该是点赞之类的。但这样做的前提是必须要有个账号。为了点赞去注册一个账号，不合适。所以另辟蹊径，想了这么一个办法。

说明：
- 各类别按时间顺序添加，排名不分先后
- 记录的是下次可能还要查的信息。那种查一下知道就行了的没有记录
- 每篇分享10个链接

## 技术类
- [https://blog.csdn.net/upgroup/article/details/81052047](https://blog.csdn.net/upgroup/article/details/81052047)  
  IntelliJ IDEA 设置 VM 运行参数。

- [https://blog.csdn.net/weixin_33720452/article/details/89623125](https://blog.csdn.net/weixin_33720452/article/details/89623125)  
  Java VM 启动参数详解

- [https://www.jianshu.com/p/d589a04aa2ba](https://www.jianshu.com/p/d589a04aa2ba)  
  Java enum switch 的使用。case 直接使用 MONDAY，而不是 Week.MONDAY 

- [https://blog.csdn.net/u011936655/article/details/86173862](https://blog.csdn.net/u011936655/article/details/86173862)  
  给BigDecimal取相反数。* -1 很容易想到，但写多了，每次都要new一个 -1，就厌烦了。有个隐藏技能：`xx.negate()`

- [https://www.cnblogs.com/lihaoyang/p/6913295.html](https://www.cnblogs.com/lihaoyang/p/6913295.html)  

  Java 枚举（Enum）和常量

- [https://blog.csdn.net/ajianyingxiaoqinghan/article/details/79682147](https://blog.csdn.net/ajianyingxiaoqinghan/article/details/79682147)

  递归与回溯的区别。 回溯是一种递归。相同点：都会调用自身。不同点：简单递归是一路到底；回溯，重在“回”字，此路不通，要回到上一个分叉点，尝试其他的分支。

- [https://blog.csdn.net/qq_39362996/article/details/82907320](https://blog.csdn.net/qq_39362996/article/details/82907320)

  用 Eclipse 生成 jar 包。

## 生活类

## 工具/App类

- [https://www.jianshu.com/p/7ddbb7dc8fec](https://www.jianshu.com/p/7ddbb7dc8fec)

  用 Typora 画流程图等。

## 其他
- [https://zhidao.baidu.com/question/1994138284644938227.html](https://zhidao.baidu.com/question/1994138284644938227.html)  
Windows激活。如果你的电脑（Windows系统）提示“许可证即将过期”，可以试试这个。第二个回答更好用。点赞也最多，不知道怎么排序的……
- [https://jingyan.baidu.com/article/f79b7cb33ceae39145023e53.html](https://jingyan.baidu.com/article/f79b7cb33ceae39145023e53.html)  
Windows 系统自带截图功能： Fn + PrtSc


